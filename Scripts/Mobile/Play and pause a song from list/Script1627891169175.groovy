import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import common.apkPath

//App version
'version UAMPMusicPlayerApp'

'Uninstall the previous application and fresh install'
apkPath.App()

'Wait for element Present of header'
Mobile.waitForElementPresent(findTestObject('Mobile/Player header'), 0)

'Tap on Generes'
Mobile.waitForElementPresent(findTestObject('Mobile/Genres'), 0)
Mobile.tap(findTestObject('Mobile/Genres'), 0)

'Tap on Rock'
Mobile.waitForElementPresent(findTestObject('Mobile/Rock'), 0)
Mobile.tap(findTestObject('Mobile/Rock'), 0)

'Tap on Home'
Mobile.waitForElementPresent(findTestObject('Mobile/Home'), 0)
Mobile.tap(findTestObject('Mobile/Home'), 0)
Mobile.delay(5)

'Tap on play or pause button'
Mobile.waitForElementPresent(findTestObject('Mobile/play or pause button'), 0)
Mobile.tap(findTestObject('Mobile/play or pause button'), 0)

'Tap on back button'
Mobile.waitForElementPresent(findTestObject('Mobile/back button'), 0)
Mobile.tap(findTestObject('Mobile/back button'), 0)

'Tap on back button'
Mobile.waitForElementPresent(findTestObject('Mobile/back button'), 0)
Mobile.tap(findTestObject('Mobile/back button'), 0)

Mobile.closeApplication()

