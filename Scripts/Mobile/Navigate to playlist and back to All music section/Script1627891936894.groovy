import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import common.apkPath

//App version
'version UAMPMusicPlayerApp'

'Uninstall the previous application and fresh install'
apkPath.App()

'Wait for element Present of header'
Mobile.waitForElementPresent(findTestObject('Mobile/Player header'), 0)

'Tap on burger menu'
Mobile.waitForElementPresent(findTestObject('Mobile/burger menu'), 0)
Mobile.tap(findTestObject('Mobile/burger menu'), 0)

'Tap on select playlists'
Mobile.waitForElementPresent(findTestObject('Mobile/select playlists'), 0)
Mobile.tap(findTestObject('Mobile/select playlists'), 0)
Mobile.delay(3)

'Tap on select playlists'
Mobile.waitForElementPresent(findTestObject('Mobile/Verify error'), 0)
def error = Mobile.getText(findTestObject('Mobile/Verify error'), 0)
println(error)
Mobile.verifyEqual(error, 'This is a placeholder for your application code.')
Mobile.delay(2)

'Tap on burger menu'
Mobile.waitForElementPresent(findTestObject('Mobile/burger menu'), 0)
Mobile.tap(findTestObject('Mobile/burger menu'), 0)

'Tap on select playlists'
Mobile.waitForElementPresent(findTestObject('Mobile/select all music'), 0)
Mobile.tap(findTestObject('Mobile/select all music'), 0)
Mobile.delay(2)

'Wait for element Present of Genres'
Mobile.waitForElementPresent(findTestObject('Mobile/Genres'), 0)

Mobile.closeApplication()