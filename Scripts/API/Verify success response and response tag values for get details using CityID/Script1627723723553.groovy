import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import static org.assertj.core.api.Assertions.*

'Request for input CityID'
response = WS.sendRequest(findTestObject('API/CityID', [('CityID') : '2643743']))

'Response for input CityID'
WS.verifyResponseStatusCode(response, 200)

def result = response.getResponseText()
println(result)

assertThat(response.getResponseText()).contains("London")

def slurper = new groovy.json.JsonSlurper()
def res = slurper.parseText(response.getResponseBodyContent())

def lon = res.coord.lon
println(lon)
WS.verifyElementPropertyValue(response, 'coord.lon', "${lon}")

def lat = res.coord.lat
println(lat)
WS.verifyElementPropertyValue(response, 'coord.lat', "${lat}")

def weatherID = res.weather[0].id
println(weatherID)
WS.verifyElementPropertyValue(response, 'weather[0].id', "${weatherID}")

def weatherMain = res.weather[0].main
println(weatherMain)
WS.verifyElementPropertyValue(response, 'weather[0].main', "${weatherMain}")

def weatherDesc = res.weather[0].description
println(weatherDesc)
WS.verifyElementPropertyValue(response, 'weather[0].description', "${weatherDesc}")

def weatherIcon = res.weather[0].icon
println(weatherIcon)
WS.verifyElementPropertyValue(response, 'weather[0].icon', "${weatherIcon}")

def base = res.base
println(base)
WS.verifyElementPropertyValue(response, 'base', "${base}")