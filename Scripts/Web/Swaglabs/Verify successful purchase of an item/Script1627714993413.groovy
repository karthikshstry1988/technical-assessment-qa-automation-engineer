import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import common.web_login
import common.web_logout
import common.first_name as FirstName
import common.first_name as LastName
import common.pincode

def first_name = FirstName.getname()
def last_name = LastName.getname()
def postalcode = pincode.getpincode()

web_login.Login()

WebUI.waitForElementPresent(findTestObject('Web/add to cart button'), 0)
WebUI.click(findTestObject('Web/add to cart button'))
WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Web/shopping_cart_link'), 0)
WebUI.click(findTestObject('Web/shopping_cart_link'))
WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Web/checkout'), 0)
WebUI.click(findTestObject('Web/checkout'))
WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Web/first-name'), 0)
WebUI.setText(findTestObject('Web/first-name'), first_name)
WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Web/last-name'), 0)
WebUI.setText(findTestObject('Web/last-name'), last_name)
WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Web/postal-code'), 0)
WebUI.setText(findTestObject('Web/postal-code'), postalcode)
WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Web/continue button'), 0)
WebUI.click(findTestObject('Web/continue button'))
WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Web/finish'), 0)
WebUI.click(findTestObject('Web/finish'))
WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('Web/back-to-products'), 0)
WebUI.click(findTestObject('Web/back-to-products'))
WebUI.delay(2)

web_logout.Logout()
