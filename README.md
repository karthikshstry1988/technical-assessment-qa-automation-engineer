# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version Katalon studio 7.2.6

### How do I get set up? ###

For Mobile Apps:

1.	Install Node.js using https://nodejs.org/en/download
2.	Install Appium: 1.12.1 onwards using below steps
3.	Install Genymotion with Virtual box using https://www.genymotion.com/fun-zone/ (Register for Personal use)
4.	Install Katalon using https://www.katalon.com/download/ (Register and Activate)
 
Steps to install Appium:
 
1.	Open Node.js Command prompt
2.	npm config set registry="http://registry.npmjs.org/"
3.	npm install
4.	npm install -g appium
5.	npm update -g
6.	node -v (optional)
7.	npm -v (optional)
8.	npm install npm@latest -g (optional)
 
Troubleshoot:  (Optional)
 
1.	npm install -g appium-doctor
2.	npm install appium-chromedriver@4.13.0
3.	npm config set registry=http://chromedriver.storage.googleapis.com/2.46/chromedriver_win32.zip/
4.	npm cache clean --force
5.	npm install -g appium-doctor
 
For Mobile:

Update settings in Katalon studio:
 
Go to Project > Settings > Desired Capabilities >  Mobile > Android and select the device and Add below configurations,
 
appWaitActivity - *
deviceReadyTimeout – 1000
appWaitDuration - 20000
 
Go to the Windows > Katalon Studio Preferences > Katalon > Mobile 
 
1.	Set Appium Directory to the installed folder. By default, it’s usually installed at C:\Users\{your login account}\AppData\Roaming\npm\node_modules\appium
2.	Set Appium Log Level to Debug
 
Genymotion:
 
1.	Open Simulator and Mobile device
2.	On Command prompt, navigate to platform-tools in C:\Users\{UserName}\.katalon\tools\android_sdk\platform-tools
3.	Type in: adb devices and observe devices listed there

For Webservices/ API:

For SSL Certificate validation, please enable Bypass certificate validation in Project/Settings -> Network.

For Web:

Update settings in Katalon studio:
 
Go to Project > Settings > Desired Capabilities > Web UI > Chrome and Add below configurations,

Below type is Boolean:

useAutomationExtension = false 
--config -webui.autoUpdateDrivers = true 
CapabilityType.ACCEPT_SSL_CERTS = true 

platform = Windows 10 (String)
version = 78.0 (Number)
args: (Dictionary)

Below type is String:

--start-maximized
--disable-plugins
--enable-precise-memory-info
--disable-popup-blocking
--disable-default-apps
--disable-extensions
--disable-infobars
--no-sandbox
--test-type
--proxy-pac-url
--proxy-auto-detect

prefs: (Dictionary)

Below type is Boolean:

download.prompt_for_download = false
safebrowsing.enabled = false
safebrowsing-disable-download-protection = true
download.directory_upgrade = true

### Contribution guidelines ###

Email settings in Katalon:

Host:
SMTP details are below,

For Gmail:

Host: smtp.gmail.com
Port: 465

For Reports : Add Basic report plugin to generate reports in PDF, HTML, Excel and Junit report

### Who do I talk to? ###

* Repo owner or admin - Karthik Shastry