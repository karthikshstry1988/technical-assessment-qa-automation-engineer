<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Web</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>02a602b1-995e-47ac-b7a9-013b81b67f02</testSuiteGuid>
   <testCaseLink>
      <guid>8996a1a7-b037-4f0a-8770-1408a8ac5508</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/Swaglabs/Verify error message for locked out user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0511ce7-e92d-48c7-a828-daec0b6bd7b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/Swaglabs/Verify login into Swaglabs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34af1599-1f37-418e-8ae4-7e1748347172</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/Swaglabs/Verify logout from Swaglabs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de0ea6ad-e87c-4cb9-a267-a2772ff5afe1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/Swaglabs/Verify successful purchase of an item</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
