<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>API</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cc486a0b-b5c5-49d8-945e-e90530ba9659</testSuiteGuid>
   <testCaseLink>
      <guid>cb524aae-6444-4612-b598-a3125c11d54d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Verify Bbox is mandatory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>434f4e28-332f-460b-8727-04e510b66e89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Verify response using invalid API key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0698b083-86a0-45af-955d-40e212f53f10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Verify response using invalid City name</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff9505b3-c173-49ab-b7f6-661e4dee8f58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Verify response using invalid cnt value</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d21fcf91-ce4f-4125-ac93-88b73ed1450f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Verify response using invalid zipcode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24c31101-31a1-4ec5-ba4b-4fa48d60e579</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Verify success response and response tag values for get details using CityID</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
