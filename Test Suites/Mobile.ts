<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Mobile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>55f85186-e16a-4a28-9d57-0155783e487b</testSuiteGuid>
   <testCaseLink>
      <guid>d8926918-d9f2-4fea-8f6b-e05615da66a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Navigate to playlist and back to All music section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cff9375c-6df4-46bf-bec4-cfe97975165f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Play and pause a song from list</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
