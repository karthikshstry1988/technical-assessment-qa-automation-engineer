package common

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject

import internal.GlobalVariable



public class first_name {

	public static void main(String[] args){
		getname();
	}

	private static String[] Firstname = [
		"Michel",
		"Sara",
		"Lincon",
		"Bellick",
		"Tbag",
		"Sucre",
		"Veronica",
		"LJ",
		"Killerman",
		"Mahone",
		"Henry"
	]

	private static Random rand = new Random();

	public static String getname() {

		def name = Firstname[rand.nextInt(Firstname.length)]

		return name
		println(name)
	}
}
